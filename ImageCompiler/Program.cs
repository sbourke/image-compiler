﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace ImageCompiler
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Add test images in z-index priority
            List<String> imageNames = new List<String>();
            imageNames.Add("Resources//F.png");
            imageNames.Add("Resources//L.png");
            imageNames.Add("Resources//C.png");
            imageNames.Add("Resources//H.png");

            Int32 imageWidth = 0;
            Int32 imageHeight = 0;
            Bitmap outputImage = null;
            Byte[] outputValues = null;

            foreach (String imageName in imageNames)
            {
                // Load the image
                Bitmap image = new Bitmap(imageName);

                // Get the output ready here
                if (outputImage == null)
                {
                    imageWidth = image.Width;
                    imageHeight = image.Height;
                    outputImage = new Bitmap(imageWidth, imageHeight);
                }

                // Load in the image pixel data
                Rectangle imageRect = new Rectangle(0, 0, imageWidth, imageHeight);
                BitmapData imageData = image.LockBits(imageRect, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
                IntPtr ptr = imageData.Scan0;

                // Prepare array to copy pixel data to
                Int32 bytes = imageData.Stride * imageHeight;
                Byte[] rgbaValues = new Byte[bytes];

                // Prepare output image pixel data array
                if (outputValues == null)
                {
                    outputValues = new Byte[bytes];
                }

                // Copy image pixel bytes to array
                Marshal.Copy(ptr, rgbaValues, 0, bytes);

                // Loop through the image's pixels
                Int32 count = 0;
                Int32 stride = imageData.Stride;
                for (Int32 column = 0; column < imageHeight; column++)
                {
                    for (Int32 row = 0; row < imageWidth; row++)
                    {
                        var b = (Byte)(rgbaValues[(column * stride) + (row * 4)]);
                        var g = (Byte)(rgbaValues[(column * stride) + (row * 4) + 1]);
                        var r = (Byte)(rgbaValues[(column * stride) + (row * 4) + 2]);
                        var a = (Byte)(rgbaValues[(column * stride) + (row * 4) + 3]);

                        // If the alpha channel has a value, overwrite the previous value
                        if(a > 0) {
                            outputValues[(column * stride) + (row * 4)] = b;
                            outputValues[(column * stride) + (row * 4) + 1] = g;
                            outputValues[(column * stride) + (row * 4) + 2] = r;
                            outputValues[(column * stride) + (row * 4) + 3] = a;
                        }
                    }
                }
            }

            Rectangle outputImageRect = new Rectangle(0, 0, imageWidth, imageHeight);
            BitmapData outputImageData = outputImage.LockBits(outputImageRect, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);

            // Copy the output array into the new image's memory space
            Marshal.Copy(outputValues, 0, outputImageData.Scan0, outputImageData.Stride * imageHeight);

            // Save output
            outputImage.Save("Output.png", ImageFormat.Png);
        }
    }
}
